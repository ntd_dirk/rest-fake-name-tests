package org.achumakin.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NameModel {

    private String firstName;
    private String lastName;
    private String prefix;

}
